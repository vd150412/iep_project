﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace IepAuctions.Hubs
{
    public class CounterHub : Hub
    {
        static long counter = 0;
        static long clicks = 0;

        public void UpdateClick()
        {
            clicks++;
            Clients.All.UpdateClick(clicks);
        }

        public override Task OnConnected()
        {
            counter++;
            Clients.All.UpdateCount(counter);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            counter--;
            Clients.All.UpdateCount(counter);
            return base.OnDisconnected(stopCalled);
        }

    }
}