﻿using IepAuctions.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace IepAuctions.Hubs
{
    public class BidHub : Hub
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        //private static int num = 0;
        //public void sendBid(string email, string auction_id) {
        //    var user = db.Users.Where(m => m.Email == email).First();
        //    user.Tokens++;
        //    db.SaveChanges();
        //    Clients.All.newBid(num);
        //}

        public void SendBid(string email, string auctionId, string price)
        {
            if (String.IsNullOrEmpty(email)) return;

            int tokensToBid = Convert.ToInt32(price);
            Guid auctionGuid = new Guid(auctionId);

            var user = db.Users.Where(u => u.Email == email).First();
            var auction = db.Auctions.Where(a => a.auction_id == auctionGuid).First();

            if(DateTime.UtcNow > auction.date_closed || auction.status != 1) {
                return;
            }

            if(!String.IsNullOrEmpty(auction.last_bidder) && 
                user.Id.Equals(auction.last_bidder) &&
                user.Tokens + auction.current_price < tokensToBid) {
                Clients.Caller.errorTokens("Buy more tokens to bid!");
                return;
            }
            else if(user.Tokens < tokensToBid) {
                Clients.Caller.errorTokens("Buy more tokens to bid!");
                return;
            }

            if(auction.current_price >= tokensToBid)
            {
                Clients.Caller.errorTokens("You are not the highest bidder!");
                return;
            }

            Bid bid = new Bid();
            bid.Auction = auction;
            bid.auction_id = auction.auction_id;
            bid.date_created = DateTime.UtcNow;
            bid.user_id = user.Id;
            bid.num_of_tokens = tokensToBid;

            if(!String.IsNullOrEmpty(auction.last_bidder))
            {
                var refundUser = db.Users.Find(auction.last_bidder);
                refundUser.Tokens += (int)auction.current_price;
            }

            auction.current_price = tokensToBid;

            user.Tokens -= (int)auction.current_price;
            auction.last_bidder = user.Id;
            auction.Bids.Add(bid);

            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            db.Entry(auction).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch(DbUpdateConcurrencyException ex)
            {
                var data = ex.Entries.Single();
                data.OriginalValues.SetValues(data.GetDatabaseValues());
                Clients.Caller.errorTokens("Concurrency error. Please try again");
            }

            string scaledPrice = ((double)(auction.token_value * auction.current_price)).ToString("0.00");
            string priceStr = auction.currency + " " +  scaledPrice;

            Clients.All.newBid(user.Email, auction.auction_id, priceStr, tokensToBid);
        }
    }
}