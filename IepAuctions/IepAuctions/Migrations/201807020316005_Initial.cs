namespace IepAuctions.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Auction",
                c => new
                    {
                        auction_id = c.Guid(nullable: false),
                        product_name = c.String(maxLength: 100, unicode: false),
                        product_image = c.Binary(),
                        duration = c.Int(),
                        start_price = c.Int(),
                        current_price = c.Int(),
                        date_created = c.DateTime(),
                        date_opened = c.DateTime(),
                        date_closed = c.DateTime(),
                        status = c.Int(),
                        last_bidder = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.auction_id);
            
            CreateTable(
                "dbo.Bid",
                c => new
                    {
                        bid_id = c.Int(nullable: false, identity: true),
                        num_of_tokens = c.Int(),
                        date_created = c.DateTime(),
                        user_id = c.String(nullable: false, maxLength: 128),
                        auction_id = c.Guid(),
                    })
                .PrimaryKey(t => t.bid_id)
                .ForeignKey("dbo.Auction", t => t.auction_id)
                .Index(t => t.auction_id);
            
            CreateTable(
                "dbo.Packets",
                c => new
                    {
                        packet_id = c.Int(nullable: false, identity: true),
                        packet_name = c.String(maxLength: 20, unicode: false),
                        num_of_tokens = c.Int(),
                    })
                .PrimaryKey(t => t.packet_id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.SystemParameters",
                c => new
                    {
                        params_id = c.Int(nullable: false, identity: true),
                        auctions_per_page = c.Int(),
                        auctions_duration = c.Int(),
                        currency = c.String(maxLength: 20, unicode: false),
                        token_value = c.Decimal(precision: 10, scale: 3),
                    })
                .PrimaryKey(t => t.params_id);
            
            CreateTable(
                "dbo.TokenOrder",
                c => new
                    {
                        order_id = c.Guid(nullable: false),
                        num_of_tokens = c.Int(),
                        packet_price = c.Decimal(precision: 10, scale: 3),
                        packet_name = c.String(maxLength: 100, unicode: false),
                        order_status = c.Int(),
                        date_created = c.DateTime(),
                        date_confirmed = c.DateTime(),
                        user_id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.order_id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Surname = c.String(),
                        Tokens = c.Int(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Bid", "auction_id", "dbo.Auction");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Bid", new[] { "auction_id" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.TokenOrder");
            DropTable("dbo.SystemParameters");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Packets");
            DropTable("dbo.Bid");
            DropTable("dbo.Auction");
        }
    }
}
