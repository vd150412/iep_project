﻿using Microsoft.Owin;
using Owin;
using IepAuctions.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.SignalR;
using Hangfire;

[assembly: OwinStartupAttribute(typeof(IepAuctions.Startup))]
namespace IepAuctions
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
            createRolesAndUsers();

            GlobalConfiguration.Configuration.UseSqlServerStorage("Auction2018");
            app.UseHangfireServer();
            app.UseHangfireDashboard();
        }

        public void createRolesAndUsers()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);



                var user = new ApplicationUser();
                user.Name = "Admin";
                user.Surname = "Admin";
                user.UserName = "iepadmin@gmail.com";
                user.Email = "iepadmin@gmail.com";

                string password = "Iep.123";

                var chkUser = userManager.Create(user, password);

                if (chkUser.Succeeded)
                {
                    var result = userManager.AddToRole(user.Id, "Admin");
                }


                //adding system parameters
                SystemParameter sysParams = new SystemParameter();
                sysParams.auctions_duration = 600;
                sysParams.auctions_per_page = 10;
                sysParams.currency = "RSD";
                sysParams.token_value = 3;
                sysParams.tokens_silver = 30;
                sysParams.tokens_gold = 50;
                sysParams.tokens_platinum = 100;
                db.SystemParameters.Add(sysParams);
                db.SaveChanges();
            }

            if (!roleManager.RoleExists("User"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();

                role.Name = "User";
                roleManager.Create(role);
            }
        }
    }
}
