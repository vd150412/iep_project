﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IepAuctions.Models;

namespace IepAuctions.Controllers
{
    public class SystemParametersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SystemParameters
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View(db.SystemParameters.ToList().First());
        }

        // GET: SystemParameters/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemParameter systemParameter = db.SystemParameters.Find(id);
            if (systemParameter == null)
            {
                return HttpNotFound();
            }
            return View(systemParameter);
        }

        // POST: SystemParameters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "params_id,auctions_per_page,auctions_duration,currency,token_value,tokens_silver,tokens_gold,tokens_platinum")] SystemParameter systemParameter)
        {
            string errorMessage = "";
            bool canModify = true;
            if (ModelState.IsValid)
            {
                if (systemParameter.tokens_silver >= systemParameter.tokens_gold) {
                    errorMessage += "Number of tokens for Golden pack must be greater than number of tokens for Silver pack!<br>";
                    canModify = false;
                }

                if (systemParameter.tokens_silver >= systemParameter.tokens_platinum) {
                    errorMessage += "Number of tokens for Platinum pack must be greater than number of tokens for Silver pack!<br>";
                    canModify = false;
                }

                if (systemParameter.tokens_gold >= systemParameter.tokens_platinum) {
                    errorMessage += "Number of tokens for Platinum pack must be greater than number of tokens for Golden pack!<br>";
                    canModify = false;
                }

                if(canModify == true)
                {
                    db.Entry(systemParameter).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ErrorMessage = errorMessage + "<br>";
                    return View(systemParameter);
                }
            }


            return View(systemParameter);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
