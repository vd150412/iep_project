﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IepAuctions.Models;
using Microsoft.AspNet.Identity;
using PagedList;
using Hangfire;
using System.Data.Entity.Infrastructure;

namespace IepAuctions.Controllers
{
    public class AuctionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: Auctions
        public ActionResult Index(String searchString, string auctionStatus, int? minPrice, int? maxPrice, int? page)
        {
            ViewBag.searchString = null;
            ViewBag.minPrice = null;
            ViewBag.maxPrice = null;
            ViewBag.auctionStatus = "-1";

            var auctions = from a in db.Auctions
                           select new AuctionSearch
                           {
                               auction_id = a.auction_id,
                               product_name = a.product_name,
                               product_image = a.product_image,
                               duration = a.duration,
                               start_price = a.start_price,
                               current_price = a.current_price,
                               status = a.status,
                               date_created = a.date_created,
                               date_opened = a.date_opened,
                               last_bidder = a.last_bidder,
                               currency = a.currency,
                               token_value = a.token_value
                           };

            if (!String.IsNullOrEmpty(searchString))
            {
                var strings = searchString.Split(' ');
                auctions = auctions.Where(a => strings.Any(val => a.product_name.Contains(val)));
                ViewBag.searchString = searchString;
            }

            if (auctionStatus != null && auctionStatus != "-1")
            {
                ViewBag.auctionStatus = auctionStatus;
                auctions = auctions.Where(a => auctionStatus == a.status.ToString());
            }

            if (minPrice != null)
            {
                ViewBag.minPrice = minPrice;
                auctions = auctions.Where(a => a.current_price >= minPrice);
            }

            if (maxPrice != null)
            {
                ViewBag.maxPrice = maxPrice;
                auctions = auctions.Where(a => a.current_price <= maxPrice);
            }

            auctions = auctions.OrderByDescending(a => a.date_created);

            int pageSize = (int)db.SystemParameters.First().auctions_per_page;
            int pageNumber = (page ?? 1);
            return View(auctions.ToPagedList(pageNumber, pageSize));
        }

        //GET: Auctions/WonAuctions
        [Authorize(Roles = "Admin,User")]
        public ActionResult WonAuctions(int? page)
        {
            var userId = User.Identity.GetUserId();
            var user = db.Users.First(u => u.Id == userId);

            var auctions = from a in db.Auctions
                           where a.last_bidder == userId
                           where a.status == 2
                           select new AuctionSearch
                           {
                               auction_id = a.auction_id,
                               product_name = a.product_name,
                               product_image = a.product_image,
                               duration = a.duration,
                               start_price = a.start_price,
                               current_price = a.current_price,
                               status = a.status,
                               date_created = a.date_created,
                               date_opened = a.date_opened,
                               last_bidder = a.last_bidder,
                               currency = a.currency,
                               token_value = a.token_value
                           };

            auctions = auctions.OrderByDescending(a => a.date_created);

            int pageSize = (int)db.SystemParameters.First().auctions_per_page;
            int pageNumber = (page ?? 1);
            return View(auctions.ToPagedList(pageNumber, pageSize));
        }

        // GET: Auctions/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auction auction = db.Auctions.Find(id);
            if (auction == null)
            {
                return HttpNotFound();
            }

            return View(auction);
        }


        // GET: Auctions/Create
        [Authorize(Roles = "User,Admin")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles ="User,Admin")]
        public ActionResult Create(AuctionCreate auctionData)
        {
            if (ModelState.IsValid)
            {
                Auction auction = new Auction();
                auction.auction_id = Guid.NewGuid();
                auction.product_name = auctionData.product_name;
                auction.start_price = auctionData.start_price;
                auction.current_price = auctionData.start_price;

                auction.date_created = DateTime.UtcNow;
                auction.status = 0; //READY

                auction.product_image = new byte[auctionData.ImageToUpload.ContentLength];
                auctionData.ImageToUpload.InputStream.Read(auction.product_image, 0, auction.product_image.Length);

                SystemParameter sysParam = db.SystemParameters.First();
                auction.currency = sysParam.currency;
                auction.token_value = sysParam.token_value;

                if (auctionData.duration == null) auction.duration = sysParam.auctions_duration;
                else auction.duration = auctionData.duration;

                db.Auctions.Add(auction);

                try
                {
                    db.SaveChanges();
                }
                catch(DbEntityValidationException ex)
                {
                    foreach (var EntityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach(var validatinoError in EntityValidationErrors.ValidationErrors)
                        {
                            Response.Write("property: " + validatinoError.PropertyName + " error: " + validatinoError.ErrorMessage);
                        }
                    }

                    return View();
                }


                logger.Info("Created auction " + auction.auction_id);
                return RedirectToAction("Details", new { id = auction.auction_id });
            }

            return View(auctionData);
        }

        //public ActionResult Create([Bind(Include = "auction_id,product_name,product_image,duration,start_price,current_price,date_created,date_opened,date_closed,status,last_bidder")] Auction auction)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        auction.auction_id = Guid.NewGuid();
        //        db.Auctions.Add(auction);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(auction);
        //}


        //// GET: Auctions/Edit/5
        //public ActionResult Edit(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Auction auction = db.Auctions.Find(id);
        //    if (auction == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(auction);
        //}

        //// POST: Auctions/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "auction_id,product_name,product_image,duration,start_price,current_price,date_created,date_opened,date_closed,status,last_bidder")] Auction auction)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(auction).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(auction);
        //}

        //// GET: Auctions/Delete/5
        //public ActionResult Delete(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Auction auction = db.Auctions.Find(id);
        //    if (auction == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(auction);
        //}

        //// POST: Auctions/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(Guid id)
        //{
        //    Auction auction = db.Auctions.Find(id);
        //    db.Auctions.Remove(auction);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



        // GET: Auctions/OpenAuction/5
        [System.Web.Mvc.Authorize(Roles = "Admin")]
        public ActionResult OpenAuction(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auction auction = db.Auctions.Find(id);
            if (auction == null)
            {
                return HttpNotFound();
            }
            if (auction.status == 0)
            {
                auction.status = 1;
                auction.date_opened = DateTime.UtcNow;
                auction.date_closed = DateTime.UtcNow.AddSeconds((double)auction.duration);

                db.Entry(auction).State = EntityState.Modified;
                db.SaveChanges();

                BackgroundJob.Schedule(() => closeAuction(auction.auction_id), TimeSpan.FromSeconds((double)auction.duration + 0.5));
            }
            return RedirectToAction("Index");
        }

        public void closeAuction(Guid id)
        {
            var auctions = db.Auctions.Where(auc => auc.auction_id == id);
            if (!auctions.Any()) return;

            var auction = auctions.First();
            if (auction != null && auction.date_closed < DateTime.UtcNow && auction.status != 2)
            {
                auction.status = 2;
                bool saveFailed;
                do
                {
                    saveFailed = false;

                    try { db.SaveChanges(); }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;
                        var entry = ex.Entries.Single();
                        entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                    }
                } while (saveFailed);
            }
        }

    }
}
