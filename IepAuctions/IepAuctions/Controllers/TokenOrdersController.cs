﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IepAuctions.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using PagedList;

namespace IepAuctions.Controllers
{
    public class TokenOrdersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: TokenOrders
        [Authorize(Roles = "User,Admin")]
        public ActionResult Index(int? page)
        {
            string user_id = User.Identity.GetUserId();
            var orders = from o in db.TokenOrders
                         where o.user_id == user_id
                         select new TokenOrderViewModel
                         {
                             order_id = o.order_id,
                             num_of_tokens = o.num_of_tokens,
                             packet_price = o.packet_price,
                             packet_name = o.packet_name,
                             order_status = o.order_status,
                             date_created = o.date_created,
                             date_confirmed = o.date_confirmed
                         };

            orders = orders.OrderByDescending(o => o.date_created);
            int pageSize = (int)db.SystemParameters.First().auctions_per_page;
            int pageNumber = (page ?? 1);
            return View(orders.ToPagedList(pageNumber, pageSize));
        }


        [HttpGet]
        [AllowAnonymous]
        public HttpStatusCodeResult CentiliPayment(string clientid, string status, int amount)
        {
            Guid guidOrder = new Guid(clientid);
            var order = db.TokenOrders.Find(guidOrder);
            if (order == null || order.order_status != 0) return new HttpStatusCodeResult(406);

            order.date_confirmed = DateTime.UtcNow;
            bool goodAmount = amount == (int)order.num_of_tokens;
            if (status == "success" &&  goodAmount)
            {
                order.order_status = 2;
                var user = db.Users.Find(order.user_id);

                user.Tokens += (int)order.num_of_tokens;
                db.SaveChanges();

                //slanje mejla
                var msg = new MailMessage();
                msg.To.Add(new MailAddress(user.Email));
                msg.From = new MailAddress("iep.etf.2018@gmail.com");

                msg.Subject = "Aucitons Portal - Token Order";
                msg.Body = "You have successfully purchased " + order.num_of_tokens + " tokens!";

                SmtpClient smtp = new SmtpClient("smtp.gmail.com");
                smtp.Port = 587;
                smtp.Credentials = new NetworkCredential("iep.etf.2018@gmail.com", "iepetf2018");
                smtp.EnableSsl = true;
                smtp.Send(msg);
                logger.Info("Order " + order.order_id + " - successfull purchase");
            }
            else
            {
                logger.Info("Order " + order.order_id + " canceled");
                order.order_status = 1;
                db.SaveChanges();
                return new HttpStatusCodeResult(406);
            }


            return new HttpStatusCodeResult(200);
        }



        //// GET: TokenOrders/Details/5
        //public ActionResult Details(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    TokenOrder tokenOrder = db.TokenOrders.Find(id);
        //    if (tokenOrder == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tokenOrder);
        //}

        // GET: TokenOrders/Create
        [Authorize(Roles = "User,Admin")]
        public ActionResult Create()
        {
            SystemParameter sp = db.SystemParameters.First();
            ViewBag.TokensSilver = sp.tokens_silver;
            ViewBag.TokensGold = sp.tokens_gold;
            ViewBag.TokensPlatinum = sp.tokens_platinum;
            ViewBag.TokenValue = sp.token_value;
            var uid = User.Identity.GetUserId();
            var user = db.Users.Where(u => u.Id == uid).First();
            ViewBag.Tokens = user.Tokens;
            return View();
        }

        // POST: TokenOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User,Admin")]
        public ActionResult Create([Bind(Include = "package_type")] TokenCreate tokenCreate)
        {
            if (ModelState.IsValid)
            {
                TokenOrder order = new TokenOrder();
                SystemParameter sysParams = db.SystemParameters.First(); 

                order.order_id = Guid.NewGuid();
                order.date_created = DateTime.UtcNow;
                order.order_status = 0;

                order.user_id = User.Identity.GetUserId();
                order.packet_name = tokenCreate.package_type;


                if(tokenCreate.package_type == "SILVER")
                {
                    order.packet_price = 22;
                    order.num_of_tokens = sysParams.tokens_silver;
                }
                else if(tokenCreate.package_type == "GOLD")
                {
                    order.packet_price = 30;
                    order.num_of_tokens = sysParams.tokens_gold;
                }
                else if(tokenCreate.package_type == "PLATINUM")
                {
                    order.packet_price = 40;
                    order.num_of_tokens = sysParams.tokens_platinum;
                }


                db.TokenOrders.Add(order);
                db.SaveChanges();

                int amount = (int)order.packet_price;
                string baseUrl = "http://stage.centili.com/payment/widget?apikey=f8428e6e027977fcf9762a2ff9f822c2&country=rs";
                string parameters = "&reference=" + order.order_id + "&price=" + amount;
                return Redirect(baseUrl + parameters);
                //string baseUrl = "http://stage.centili.com/widget/WidgetModule?api=f8428e6e027977fcf9762a2ff9f822c2";
                //string parameters = "&clientId=" + order.order_id + "&item_name=" + order.packet_name + "&country=rs&countrylock=true&amount=" + amount;
                //return Redirect(baseUrl + parameters);
            }

            return View(tokenCreate);
        }

        //// GET: TokenOrders/Edit/5
        //public ActionResult Edit(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    TokenOrder tokenOrder = db.TokenOrders.Find(id);
        //    if (tokenOrder == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tokenOrder);
        //}

        //// POST: TokenOrders/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "order_id,num_of_tokens,packet_price,packet_name,order_status,date_created,date_confirmed,user_id")] TokenOrder tokenOrder)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(tokenOrder).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(tokenOrder);
        //}

        //// GET: TokenOrders/Delete/5
        //public ActionResult Delete(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    TokenOrder tokenOrder = db.TokenOrders.Find(id);
        //    if (tokenOrder == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tokenOrder);
        //}

        //// POST: TokenOrders/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(Guid id)
        //{
        //    TokenOrder tokenOrder = db.TokenOrders.Find(id);
        //    db.TokenOrders.Remove(tokenOrder);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
