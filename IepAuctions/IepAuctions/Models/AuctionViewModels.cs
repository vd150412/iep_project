﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IepAuctions.Models
{
    public partial class AuctionSearch
    {
        public Guid auction_id { get; set; }

        public string product_name { get; set; }

        public byte[] product_image { get; set; }

        public int? duration { get; set; }

        public int? start_price { get; set; }

        public int? current_price { get; set; }

        public DateTime? date_created { get; set; }

        public DateTime? date_opened { get; set; }

        public int? status { get; set; }

        public string last_bidder { get; set; }

        public string currency { get; set; }

        public decimal? token_value { get; set; }
    }

    public partial class AuctionCreate
    {
        //public int ID { get; set; }

        [Required]
        [Display(Name = "Product name")]
        public string product_name { get; set; }

        [Display(Name = "Duration(seconds)")]
        [Range(5, int.MaxValue)]
        public int? duration { get; set; }

        [Required]
        [Display(Name = "Starting price")]
        [Range(0, int.MaxValue)]
        public int start_price { get; set; }


        [Required]
        [Display(Name = "Image")]
        public HttpPostedFileBase ImageToUpload { get; set; }

    }
}