﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IepAuctions.Models
{
    public class TokenCreate
    {
        public string package_type { set; get; }
    }

    public partial class TokenOrderViewModel
    {
        [Key]
        public Guid order_id { get; set; }

        public int? num_of_tokens { get; set; }

        public decimal? packet_price { get; set; }

        [StringLength(100)]
        public string packet_name { get; set; }

        public int? order_status { get; set; }

        public DateTime? date_created { get; set; }

        public DateTime? date_confirmed { get; set; }

    }
}