namespace IepAuctions.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SystemParameter
    {
        [Key]
        public int params_id { get; set; }

        [Required]
        [Range(0, 1000)]
        public int? auctions_per_page { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int? auctions_duration { get; set; }

        [Required]
        [StringLength(20)]
        public string currency { get; set; }

        [Required]
        [Range(0.0, Double.MaxValue)]
        public decimal? token_value { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int? tokens_silver { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int? tokens_gold { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int? tokens_platinum { get; set; }
    }
}
