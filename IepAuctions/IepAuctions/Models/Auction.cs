namespace IepAuctions.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Auction")]
    public partial class Auction
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Auction()
        {
            Bids = new HashSet<Bid>();
        }

        [Key]
        public Guid auction_id { get; set; }

        [StringLength(100)]
        public string product_name { get; set; }

        public byte[] product_image { get; set; }

        public int? duration { get; set; }

        public int? start_price { get; set; }

        public int? current_price { get; set; }

        public DateTime? date_created { get; set; }

        public DateTime? date_opened { get; set; }

        public DateTime? date_closed { get; set; }

        public int? status { get; set; }

        [StringLength(128)]
        public string last_bidder { get; set; }

        [StringLength(20)]
        public string currency { get; set; }

        public decimal? token_value { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bid> Bids { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
