namespace IepAuctions.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Bid")]
    public partial class Bid
    {
        [Key]
        public int bid_id { get; set; }

        public int? num_of_tokens { get; set; }

        public DateTime? date_created { get; set; }

        [Required]
        [StringLength(128)]
        public string user_id { get; set; }

        public Guid? auction_id { get; set; }

        public virtual Auction Auction { get; set; }
    }
}
