namespace IepAuctions.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TokenOrder")]
    public partial class TokenOrder
    {
        [Key]
        public Guid order_id { get; set; }

        public int? num_of_tokens { get; set; }

        public decimal? packet_price { get; set; }

        [StringLength(100)]
        public string packet_name { get; set; }

        public int? order_status { get; set; }

        public DateTime? date_created { get; set; }

        public DateTime? date_confirmed { get; set; }

        [Required]
        [StringLength(128)]
        public string user_id { get; set; }
    }
}
