//namespace IepAuctions.Models
//{
//    using System;
//    using System.Data.Entity;
//    using System.ComponentModel.DataAnnotations.Schema;
//    using System.Linq;

//    public partial class AuctionDb : DbContext
//    {
//        public AuctionDb()
//            : base("name=AuctionDb")
//        {
//        }

//        public virtual DbSet<Auction> Auctions { get; set; }
//        public virtual DbSet<Bid> Bids { get; set; }
//        public virtual DbSet<Packet> Packets { get; set; }
//        public virtual DbSet<SystemParameter> SystemParameters { get; set; }
//        public virtual DbSet<TokenOrder> TokenOrders { get; set; }

//        protected override void OnModelCreating(DbModelBuilder modelBuilder)
//        {
//            modelBuilder.Entity<Auction>()
//                .Property(e => e.product_name)
//                .IsUnicode(false);

//            modelBuilder.Entity<Packet>()
//                .Property(e => e.packet_name)
//                .IsUnicode(false);

//            modelBuilder.Entity<SystemParameter>()
//                .Property(e => e.currency)
//                .IsUnicode(false);

//            modelBuilder.Entity<SystemParameter>()
//                .Property(e => e.token_value)
//                .HasPrecision(10, 3);

//            modelBuilder.Entity<TokenOrder>()
//                .Property(e => e.packet_price)
//                .HasPrecision(10, 3);

//            modelBuilder.Entity<TokenOrder>()
//                .Property(e => e.packet_name)
//                .IsUnicode(false);
//        }
//    }
//}
